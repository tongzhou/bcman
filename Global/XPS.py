import types
from Global.TColor import *

XPS = types.SimpleNamespace()

def initXPSConfig():
  file = './xps.cfg'
  print("load config from", file)
  print("============= Current Configs ============")
  for line in open(file):
    if line.startswith('#'):
      continue
    if not line.strip():
      continue
    key, value = line.strip().split('=')
    print("%s: %s" % (key, value))
    setattr(XPS, key, value)
    if ' ' in value:
      setattr(XPS, key, value.split())
    else:
      setattr(XPS, key, value)

  print("==========================================")    
  print(TColor.red, end='\n')
  print("Welcome to marenashell! Make sure the paths above match with your machine!\n")
  print(TColor.end, end='')
