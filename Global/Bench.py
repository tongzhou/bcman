import os
import collections
import json
import math
from Global.XPS import *
import Utils.OS as OS

class Bench(object):
  def __init__(self, name, lang, suite):
    self.name = name
    self.lang = lang
    self.suite = suite
    self.ref_cmds = []
    self.test_cmds = []
    self.old_wd = '.'
    self.edge_bits = 0
    self.scce_acyc_bits = 0
    self.pcce_acyc_bits = 0

  def isC(self):
    return self.lang & 0b001 == 0b001

  def isPureC(self):
    return self.lang == 0b001

  def isCpp(self):
    return self.lang & 0b010 == 0b010

  def isFortran(self):
    return self.lang & 0b100 == 0b100

  def getShortName(self):
    return self.name[4:]

  def getTargetName(self, target, suf='.ll'):
    return self.name + '.' + target + suf

  def getHomeDir(self, subdir=''):
    return XPS.bench_dir + self.name + '/%s/' % subdir

  def getResultDir(self):
    return XPS.result_dir + self.name + '/'

  def getConfigDir(self, cfg, cat=''):
    return self.getResultDir() + "/" + cfg + '/' + cat + '/'

  def getConfigIterDir(self, cfg, iter, cat='ref'):
    return self.getConfigDir(cfg, cat) + '/i' + str(iter) + '/'

  def getConfigArchiveDir(self, cfg, cat='ref'):
    d = self.getConfigDir(cfg, cat) + 'archive/'
    if not os.path.isdir(d):
      os.makedirs(d)
    return d

  def getIterRuns(self, cfg, it):
    run_dirs = []
    for r in range(len(self.ref_cmds)):
      run_dirs.append(
        "%s/i%d/r%d/" % (self.getConfigDir(cfg, 'ref'), it, r)
      )
    return run_dirs

  def getCommands(self, cat='ref'):
    if cat == 'ref':
      return self.ref_cmds
    elif cat == 'test':
      return self.test_cmds
    else:
      return None
    
  def getConfigDirs(self):
    dirs = []
    root = self.getResultDir()
    for d in os.listdir(root):
      #print(d)
      if os.path.isdir(root+'/'+d):
        if d == 'input' or d == 'inputs':
          continue
        dirs.append(root+'/'+d+'/')
    return dirs
    
  def getIR(self, cfg=""):
    prefix = self.getHomeDir('ir') + self.name
    if not cfg:
      return prefix + '.ll'
    else:
      return prefix + '.' + cfg + '.ll'

  def cd(self, subdir=''):
    self.old_wd = os.getcwd()
    os.chdir(self.getHomeDir(subdir))
    print("cwd:", os.getcwd())

  def cdb(self):
    '''Change back
    '''
    os.chdir(self.old_wd)
    print("cwd:", os.getcwd())


class CPU2006(object):
  # 1: C, 2: C++, 4: Fortran, 5: C and Fortran
  all = collections.OrderedDict()
  # CINT
  all["400.perlbench"] = Bench("400.perlbench", 0b001, 0)
  all["401.bzip2"] = Bench("401.bzip2", 0b001, 0)
  all["403.gcc"] = Bench("403.gcc", 0b001, 0)
  all["429.mcf"] = Bench("429.mcf", 0b001, 0)
  all["445.gobmk"] = Bench("445.gobmk", 0b001, 0)
  all["456.hmmer"] = Bench("456.hmmer", 0b001, 0)
  all["458.sjeng"] = Bench("458.sjeng", 0b001, 0)
  all["462.libquantum"] = Bench("462.libquantum", 0b001, 0)
  all["464.h264ref"] = Bench("464.h264ref", 0b001, 0)
  all["471.omnetpp"] = Bench("471.omnetpp", 0b010, 0)
  all["473.astar"] = Bench("473.astar", 0b010, 0)
  all["483.xalancbmk"] = Bench("483.xalancbmk", 0b010, 0)

  # CFP
  all["410.bwaves"] = Bench("410.bwaves", 0b100, 1)
  all["416.gamess"] = Bench("416.gamess", 0b100, 1)
  all["433.milc"] = Bench("433.milc", 0b001, 1)
  all["434.zeusmp"] = Bench("434.zeusmp", 0b100, 1)
  all["435.gromacs"] = Bench("435.gromacs", 0b101, 1)
  all["436.cactusADM"] = Bench("436.cactusADM", 0b101, 1)
  all["437.leslie3d"] = Bench("437.leslie3d", 0b100, 1)
  all["444.namd"] = Bench("444.namd", 0b010, 1)
  all["447.dealII"] = Bench("447.dealII", 0b010, 1)
  all["450.soplex"] = Bench("450.soplex", 0b010, 1)
  all["453.povray"] = Bench("453.povray", 0b010, 1)
  all["454.calculix"] = Bench("454.calculix", 0b101, 1)
  all["459.GemsFDTD"] = Bench("459.GemsFDTD", 0b100, 1)
  all["465.tonto"] = Bench("465.tonto", 0b100, 1)
  all["470.lbm"] = Bench("470.lbm", 0b001, 1)
  all["481.wrf"] = Bench("481.wrf", 0b101, 1)
  all["482.sphinx3"] = Bench("482.sphinx3", 0b001, 1)

class CPU2017(object):
  # 1: C, 2: C++, 4: Fortran, 5: C and Fortran
  all = collections.OrderedDict()
  # CINT
  all["500.perlbench_r"] = Bench("500.perlbench_r", 0b001, 0)
  all["502.gcc_r"] = Bench("502.gcc_r", 0b001, 0)
  all["505.mcf_r"] = Bench("505.mcf_r", 0b001, 0)
  all["520.omnetpp_r"] = Bench("520.omnetpp_r", 0b010, 0)
  all["523.xalancbmk_r"] = Bench("523.xalancbmk_r", 0b010, 0)
  all["525.x264_r"] = Bench("525.x264_r", 0b001, 0)
  all["531.deepsjeng_r"] = Bench("531.deepsjeng_r", 0b010, 0)
  all["541.leela_r"] = Bench("541.leela_r", 0b010, 0)
  all["548.exchange2_r"] = Bench("548.exchange2_r", 0b100, 0)
  all["557.xz_r"] = Bench("557.xz_r", 0b001, 0)

  # CFP
  all["503.bwaves_r"] = Bench("503.bwaves_r", 0b100, 1)
  all["507.cactuBSSN_r"] = Bench("507.cactuBSSN_r", 0b111, 1)
  all["508.namd_r"] = Bench("508.namd_r", 0b010, 1)
  all["510.parest_r"] = Bench("510.parest_r", 0b010, 1)
  all["511.povray_r"] = Bench("511.povray_r", 0b011, 1)
  all["519.lbm_r"] = Bench("519.lbm_r", 0b001, 1)
  all["521.wrf_r"] = Bench("521.wrf_r", 0b101, 1)
  all["526.blender_r"] = Bench("526.blender_r", 0b011, 1)
  all["527.cam4_r"] = Bench("527.cam4_r", 0b101, 1)
  all["538.imagick_r"] = Bench("538.imagick_r", 0b001, 1)
  all["544.nab_r"] = Bench("544.nab_r", 0b001, 1)
  all["549.fotonik3d_r"] = Bench("549.fotonik3d_r", 0b100, 1)
  all["554.roms_r"] = Bench("554.roms_r", 0b100, 1)

#################### Interfaces ####################

cur_benches = []
suite = CPU2017

def setSuite(s=CPU2017):
  global suite, XPS
  suite = s
  if suite == CPU2006:
    XPS.bench_dir = XPS.cpu2006_bench_dir
    XPS.result_dir = XPS.cpu2006_result_dir
  elif suite == CPU2017:
    XPS.bench_dir = XPS.cpu2017_bench_dir
    XPS.result_dir = XPS.cpu2017_result_dir

  initBenches()  

def getBenches():
  return cur_benches

def printBenches():
  for B in getBenches():
    print(B.name)

def setBenchAcyclicBits():
  acyc_bits = json.load(open("Global/ASCCEBits.json"))
  for B in suite.all.values():
    if B.name in acyc_bits:
      B.scce_acyc_bits = acyc_bits[B.name]

  acyc_bits = json.load(open("Global/APCCEBits.json"))
  for B in suite.all.values():
    if B.name in acyc_bits:
      B.pcce_acyc_bits = acyc_bits[B.name]

  edge_bits = json.load(open("Global/CycleEdgeBits.json"))    
  for B in suite.all.values():
    if B.name in edge_bits:
      
      B.edge_bits = math.ceil(math.log2(edge_bits[B.name]+1))

def initBenches(fortran_mode=0):
  setBenchAcyclicBits()    
  for B in suite.all.values():
    # if not B.name.startswith('50'):
    #   continue

    if fortran_mode == -1:  # include all benches
      cur_benches.append(B)
    elif fortran_mode == 0:  # no fortran
      if not B.isFortran():
        cur_benches.append(B)
    elif fortran_mode == 1:  # only fortran
      if B.isFortran():
        cur_benches.append(B)

    test_cmd_file = B.getHomeDir() + '/commands/test/run'
    if os.path.isfile(test_cmd_file):  
      cmds = os.popen('cat %s/commands/test/run'
                      % (XPS.bench_dir + B.name)).read()
      inputs = [x.split(' ', 1)[1]
                for x in cmds.strip().split('\n')]
      inputs = [x.replace("2>>", "2>")
                for x in inputs]
      B.test_cmds = inputs
    
    cmds = os.popen('cat %s/commands/ref/run'
                    % (XPS.bench_dir + B.name)).read()

    inputs = [x.split(' ', 1)[1]
              for x in cmds.strip().split('\n')]
    inputs = [x.replace("2>>", "2>")
              for x in inputs]
    B.ref_cmds = inputs

  if hasattr(XPS, "excluded"):
    if type(XPS.excluded) == str:
      XPS.excluded = [XPS.excluded]
    removeBenchList(XPS.excluded)

def removeBenches(*names):
  for name in names:
    found = False
    for key in suite.all:
      if name in key:
        B = suite.all[key]
        if B in cur_benches:
          cur_benches.remove(B)
        found = True
        break
    if not found:
      print('Bench "' + name + '" not found')

def addBenches(*names):
  for name in names:
    found = False
    for key in suite.all:
      if name in key:
        B = suite.all[key]
        if B not in cur_benches:
          cur_benches.append(B)
        found = True
        break
    if not found:
      print('Bench "' + name + '" not found')

def removeBenchList(names):
  for name in names:
    removeBenches(name)

def setAllBenches():
  cur_benches.clear()
  for B in suite.all.values():
    cur_benches.append(B)

def setCBenches():
  cur_benches.clear()
  for B in suite.all.values():
    if B.isPureC():
      cur_benches.append(B)

def setCppBenches():
  cur_benches.clear()
  for B in suite.all.values():
    if B.isCpp():
      cur_benches.append(B)

def setBenches(*names):
  cur_benches.clear()
  for name in names:
    found = False
    for key in suite.all:
      if name in key:
        cur_benches.append(suite.all[key])
        found = True
        break
    if not found:
      print('Bench "' + name + '" not found')

def sb(*names):
  setBenches(*names)


#################### On Load ####################

initXPSConfig()
setSuite(suite)
# setBenches("gcc", "sjeng", "pov", "sphi")
# For ACCE
#setBenches('gcc', 'gob', "mcf", 'deal', 'soplex', 'pov', 'hmmer', 'sjeng', 'sph')
removeBenches(
  "perl",
  #"parest", "leela",
  "om")
#removeBenches("namd", "lbm")
removeBenches("blen")
addBenches('cac')
# removeBenches("perl", # llvm-link fails
# #              "gcc", # about 1% contexts undetected
#               "omnet", # ICProfile exe segfaults
#  #             "xalan", # ic-promote fails on this one
#               "pov", # implementation difficulty
#               )
printBenches()
