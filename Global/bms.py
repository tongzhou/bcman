import collections
import os
import shutil
import types
from src.flags import *


class TColor(object):
    # HEADER = '\033[95m'
    # OKBLUE = '\033[94m'
    # OKGREEN = '\033[92m'
    # WARNING = '\033[93m'
    # FAIL = '\033[91m'
    red = '\033[31m'
    end = '\033[0m'
    bold = '\033[1m'
    underline = '\033[4m'


XPS = types.SimpleNamespace()
#XPS = object()


class Bench(object):
    def __init__(self, name, lang, suite):
        self.name = name
        self.lang = lang
        self.suite = suite
        self.ref_cmds = []
        self.test_cmds = []
        self.old_wd = '.'

    def is_c(self):
        return self.lang & 0b001 == 0b001

    def is_cpp(self):
        return self.lang & 0b010 == 0b010

    def is_fortran(self):
        return self.lang & 0b100 == 0b100

    def get_short_name(self):
        return self.name[4:]

    def get_target_name(self, target, suf='.ll'):
        return self.name + '.' + target + suf

    def get_dir(self, subdir=''):
        return CPU2006.get_bench_dir(self) + '/%s/' % subdir

    def get_result_dir(self, cat='ref'):
        return XPS.result_dir + self.name + '-%s/' % cat

    def get_src_ll(self):
        return self.get_dir('ir') + self.name + '.ll'

    def get_ll(self, cfg):
        return self.get_dir('ir') + self.name + '.' + cfg + '.ll'

    def cd(self, subdir=''):
        self.old_wd = os.getcwd()
        os.chdir(self.get_dir(subdir))
        print("cwd:", os.getcwd())

    def cdb(self):
        '''Change back
        '''
        os.chdir(self.old_wd)
        print("cwd:", os.getcwd())
        

class CPU2006(object):
    all = collections.OrderedDict()
    # CINT
    all["400.perlbench"] = Bench("400.perlbench", 0b001, 0)  # 1: C, 2: C++, 4: Fortran, 5: C and Fortran
    all["401.bzip2"] = Bench("401.bzip2", 0b001, 0)
    all["403.gcc"] = Bench("403.gcc", 0b001, 0)
    all["429.mcf"] = Bench("429.mcf", 0b001, 0)
    all["445.gobmk"] = Bench("445.gobmk", 0b001, 0)
    all["456.hmmer"] = Bench("456.hmmer", 0b001, 0)
    all["458.sjeng"] = Bench("458.sjeng", 0b001, 0)
    all["462.libquantum"] = Bench("462.libquantum", 0b001, 0)
    all["464.h264ref"] = Bench("464.h264ref", 0b001, 0)
    all["471.omnetpp"] = Bench("471.omnetpp", 0b010, 0)
    all["473.astar"] = Bench("473.astar", 0b010, 0)
    all["483.xalancbmk"] = Bench("483.xalancbmk", 0b010, 0)

    # CFP
    all["410.bwaves"] = Bench("410.bwaves", 0b100, 1)
    all["416.gamess"] = Bench("416.gamess", 0b100, 1)
    all["433.milc"] = Bench("433.milc", 0b001, 1)
    all["434.zeusmp"] = Bench("434.zeusmp", 0b100, 1)
    all["435.gromacs"] = Bench("435.gromacs", 0b101, 1)
    all["436.cactusADM"] = Bench("436.cactusADM", 0b101, 1)
    all["437.leslie3d"] = Bench("437.leslie3d", 0b100, 1)
    all["444.namd"] = Bench("444.namd", 0b010, 1)
    all["447.dealII"] = Bench("447.dealII", 0b010, 1)
    all["450.soplex"] = Bench("450.soplex", 0b010, 1)
    all["453.povray"] = Bench("453.povray", 0b010, 1)
    all["454.calculix"] = Bench("454.calculix", 0b101, 1)
    all["459.GemsFDTD"] = Bench("459.GemsFDTD", 0b100, 1)
    all["465.tonto"] = Bench("465.tonto", 0b100, 1)
    all["470.lbm"] = Bench("470.lbm", 0b001, 1)
    all["481.wrf"] = Bench("481.wrf", 0b101, 1)
    all["482.sphinx3"] = Bench("482.sphinx3", 0b001, 1)

    # bench_dir = "please specify"
    # result_dir = "please specify"
    # orig_dir = "please specify"
    cur = []

    
    @staticmethod
    def set_benches(*names):
        CPU2006.cur = []
        for name in names:
            found = False
            for key in CPU2006.all:
                if name in key:
                    CPU2006.cur.append(CPU2006.all[key])
                    found = True
                    break
            if not found:
                print('Bench "'+name+'" not found')
            #CPU2006.cur.append(CPU2006.all[name])

    @staticmethod
    def remove_bench_ls(names):
        for name in names:
            CPU2006.cur.remove(CPU2006.all[name])

    @staticmethod
    def remove_benches(*names):
        for name in names:
            found = False
            for key in CPU2006.all:
                if name in key:
                    CPU2006.cur.remove(CPU2006.all[key])
                    found = True
                    break
            if not found:
                print('Bench "'+name+'" not found')


    @staticmethod
    def benches():
        for B in CPU2006.cur:
            print(B.name)

    @staticmethod
    def get_bench_names():
        names = []
        for B in CPU2006.cur:
            names.append(B.name)
        return names

    @staticmethod
    def init():
        CPU2006.init_config()
        CPU2006.init_benches(-1)

    @staticmethod
    def init_benches(fortran_mode=0):
        for B in CPU2006.all.values():
            if fortran_mode == -1:  # include all benches
                CPU2006.cur.append(B)   
            elif fortran_mode == 0:  # no fortran
                if not B.is_fortran():
                    CPU2006.cur.append(B)   
            elif fortran_mode == 1:  # only fortran
                if B.is_fortran():
                    CPU2006.cur.append(B) 

            cmds = os.popen('cat %s/commands/test/run' % (XPS.bench_dir+B.name)).read()
            inputs = [x.split(' ', 1)[1] for x in cmds.strip().split('\n')]
            inputs = [x.replace("2>>", "2>") for x in inputs]
            B.test_cmds = inputs

            cmds = os.popen('cat %s/commands/ref/run' % (XPS.bench_dir + B.name)).read()
            inputs = [x.split(' ', 1)[1] for x in cmds.strip().split('\n')]
            inputs = [x.replace("2>>", "2>") for x in inputs]
            B.ref_cmds = inputs

        # test code
        #CPU2006.set_bench_ls(['sop'])
        
        CPU2006.remove_bench_ls(XPS.excluded)
        print("benches:")
        CPU2006.benches()
        
        if UseTestSuite:
            pass
            #CPU2006.set_benches(['bzip2', 'hmmer', 'h2'])
    

    @staticmethod
    def init_config():
        print("load ./xps.cfg...")
        for line in open('./xps.cfg'):
            if line.startswith('#'):
                continue
            key, value = line.strip().split('=')
            print(key, value)
            setattr(XPS, key, value)
            if ' ' in value:
                setattr(XPS, key, value.split())
            else:
                setattr(XPS, key, value)
                
            
        print(TColor.red, end='\n')
        print("Welcome to bcman! Make sure the paths above match with your machine!")
        print(TColor.end, end='')
        

    @staticmethod
    def get_bench_dir(B):
        return XPS.bench_dir + B.name

    @staticmethod
    def get_bench_src_dir(B):
        return XPS.bench_dir + B.name + "/src/"

    @staticmethod
    def get_bench_ref_dir(B):
        return XPS.result_dir + B.name + "-ref/"

    # @staticmethod
    # def get_bench_iter_dir(B, it, runtype):
    #     return CPU2006.result_dir + B.name + "-%s/"%(runtype)

    @staticmethod
    def get_bench_target_exe(B, target):
        return CPU2006.get_bench_src_dir(B) + B.name + "." + target


### Executes on load        

CPU2006.init()



sb = CPU2006.set_benches


#
# class Bench(object):
#     def __init__(self, name, lang, suite):
#         self.name = name
#         self.lang = lang
#         self.suite = suite
#
#     def name(self):
#         return self.name
#
#     def is_c(self):
#         return self.lang & 0b001 == 0b001
#
#     def is_cpp(self):
#         return self.lang & 0b010 == 0b010
#
#     def is_fortran(self):
#         return self.lang & 0b100 == 0b100
#
#     def short_name(self):
#         return self.name[4:]
#
#
# class CPU2006(object):
#     def __init__(self):
#         self.benches = [
#             # CINT
#             Bench("400.perlbench", 0b001, 0),  # 1: C, 2: C++, 4: Fortran, 5: C and Fortran
#             Bench("401.bzip2", 0b001, 0),
#             Bench("403.gcc", 0b001, 0),
#             Bench("429.mcf", 0b001, 0),
#             Bench("445.gobmk", 0b001, 0),
#             Bench("456.hmmer", 0b001, 0),
#             Bench("458.sjeng", 0b001, 0),
#             Bench("462.libquantum", 0b001, 0),
#             Bench("464.h264ref", 0b001, 0),
#             Bench("471.omnetpp", 0b010, 0),
#             Bench("473.astar", 0b010, 0),
#             Bench("483.xalancbmk", 0b010, 0),
#
#             # CFP
#             Bench("410.bwaves", 0b100, 1),
#             Bench("416.gamess", 0b100, 1),
#             Bench("433.milc", 0b001, 1),
#             Bench("434.zeusmp", 0b100, 1),
#             Bench("435.gromacs", 0b101, 1),
#             Bench("436.cactusADM", 0b101, 1),
#             Bench("437.leslie3d", 0b100, 1),
#             Bench("444.namd", 0b010, 1),
#             Bench("447.dealII", 0b010, 1),
#             Bench("450.soplex", 0b010, 1),
#             Bench("453.povray", 0b010, 1),
#             Bench("454.calculix", 0b101, 1),
#             Bench("459.GemsFDTD", 0b100, 1),
#             Bench("465.tonto", 0b100, 1),
#             Bench("470.lbm", 0b001, 1),
#             Bench("481.wrf", 0b101, 1),
#             Bench("482.sphinx3", 0b001, 1),
#         ]
#
#         self.IngnoreFortran = True
#
#
#     def benches(self):
#         return self.benches
#
#
#
#
# CINT2006 = [
#     Bench("400.perlbench", 0b001),  # 1: C, 2: C++, 4: Fortran, 5: C and Fortran
#     Bench("401.bzip2", 0b001),
#     Bench("403.gcc", 0b001),
#     Bench("429.mcf", 0b001),
#     Bench("445.gobmk", 0b001),
#     Bench("456.hmmer", 0b001),
#     Bench("458.sjeng", 0b001),
#     Bench("462.libquantum", 0b001),
#     Bench("464.h264ref", 0b001),
#     Bench("471.omnetpp", 0b010),
#     Bench("473.astar", 0b010),
#     Bench("483.xalancbmk", 0b010),
# ]
#
# CFP2006 = [
#     Bench("410.bwaves", 0b100),
#     Bench("416.gamess", 0b100),
#     Bench("433.milc", 0b001),
#     Bench("434.zeusmp", 0b100),
#     Bench("435.gromacs", 0b101),
#     Bench("436.cactusADM", 0b101),
#     Bench("437.leslie3d", 0b100),
#     Bench("444.namd", 0b010),
#     Bench("447.dealII", 0b010),
#     Bench("450.soplex", 0b010),
#     Bench("453.povray", 0b010),
#     Bench("454.calculix", 0b101),
#     Bench("459.GemsFDTD", 0b100),
#     Bench("465.tonto", 0b100),
#     Bench("470.lbm", 0b001),
#     Bench("481.wrf", 0b101),
#     Bench("482.sphinx3", 0b001),
# ]
#
# CPU2006 = CINT2006 + CFP2006
#
# IgnoreFortran = True
#
#
# dir = "/home/tzhou/projects/xps/benchmarks/cpu2006"
