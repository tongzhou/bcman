import time
from datetime import datetime
from Global.Bench import *
import Compile.Compile as Compile
import Utils.OS as OS

PERF = "perf stat -o perf_stats.out -B -e cpu-cycles,instructions,"\
"cache-references,cache-misses,branches,branch-misses,page-faults,"\
"context-switches,cpu-migrations,minor-faults,major-faults,"\
"alignment-faults,emulation-faults,L1-dcache-loads,L1-dcache-load-misses,"\
"L1-dcache-stores,L1-dcache-store-misses,L1-dcache-prefetches,"\
"L1-dcache-prefetch-misses,L1-icache-loads,L1-icache-load-misses,"\
"LLC-loads,LLC-load-misses,LLC-stores,LLC-store-misses,LLC-prefetches,"\
"LLC-prefetch-misses,dTLB-loads,dTLB-load-misses,dTLB-stores,"\
"dTLB-store-misses,iTLB-loads,iTLB-load-misses,branch-loads,branch-load-misses"

def runCleanExps(configs, iters=range(1), runs=None, env="", prefix=PERF,
            cat='ref'):
  runExps(configs, iters, runs, env, prefix, cat, True, True)

def runTestExps(configs, iters=range(1), runs=None, env="", prefix=PERF,
                run_builds=False, run_transform=False):
  runExps(configs, iters, runs, env, prefix, 'test', run_builds, run_transform)

def runRefExps(configs, iters=range(1), runs=None, env="", prefix=PERF,
                run_builds=False, run_transform=False):
  runExps(configs, iters, runs, env, prefix, 'ref', run_builds, run_transform)

def runExps(configs, iters=range(1), runs=None, env="", prefix=PERF,
            cat='ref', run_builds=False, run_transform=False):
  if run_transform:
    Compile.runTransformConfigs(*configs)
  if run_builds:
    Compile.runBuildConfigs(*configs)
  if type(configs) == str:
    configs = [configs]

  allruns = False
  if not runs:
    allruns = True
  for i in iters:
    t1 = time.time()
    for B in getBenches():
      if allruns:
        runs = range(len(B.getCommands(cat)))
       
      print('runs:', runs)
      
      for run in runs:
        for cfg in configs:
          runExps_i(B, cfg, run, i, prefix, env, cat)
      
    t2 = time.time()
    print("iter %d takes %.3f seconds" % (i, t2 - t1))

def runExps_i(B, config, run, it, prefix, env, cat='ref'):
  acyc_slots = 0
  if 'fcce' in config:
    if B.edge_bits != 0:
      if B.pcce_acyc_bits > 128:
        acyc_slots = (192-B.pcce_acyc_bits)/B.edge_bits
      elif B.pcce_acyc_bits > 64:
        acyc_slots = (128-B.pcce_acyc_bits)/B.edge_bits
      else:
        acyc_slots = (64-B.pcce_acyc_bits)/B.edge_bits
    env += " XPS_AcycSlots=%d" % acyc_slots
  elif 'scce' in config:
    if B.edge_bits != 0:
      if B.scce_acyc_bits > 128:
        acyc_slots = (192-B.scce_acyc_bits)/B.edge_bits
      elif B.scce_acyc_bits > 64:
        acyc_slots = (128-B.scce_acyc_bits)/B.edge_bits
      else:
        acyc_slots = (64-B.scce_acyc_bits)/B.edge_bits
      
      # acyc_slots = (64-B.scce_acyc_bits)/B.edge_bits
      # if (acyc_slots < 0):
      #   acyc_slots = (128-B.scce_acyc_bits)/B.edge_bits
      # if (acyc_slots < 0):
      #   acyc_slots = (192-B.scce_acyc_bits)/B.edge_bits  
    env += " XPS_AcycSlots=%d" % acyc_slots
    
  print(env)
  iter_dir = B.getConfigDir(config, cat) + "/i%d/r%d/" % (it, run)
  OS.system("mkdir -p %s" % (iter_dir))
  ofs = open(iter_dir + 'myrun.sh', 'w')
  ofs.write('cp -r ../../../../inputs/all/* .\n')
  if cat == 'ref':
    ofs.write('cp -r ../../../../inputs/ref/* .\n')
  elif cat == 'test':
    ofs.write('cp -r ../../../../inputs/test/* .\n')
  
  run_cmd = prefix + ' env ' + env + ' ../../../' + \
                     B.name + ' ' + B.getCommands(cat)[run]

  #print(run_cmd)
  ofs.write(run_cmd + '\n')
  ofs.write('ls | grep -v ".out" | grep -v ".log" | '
            'grep -v ".err" | grep -v "run.sh" | xargs rm -r\n')
  #ofs.write('cat *.err\n')

  context_file = ''
  if 'pcce' in config:
    ofs.write('cat PCCE.out\n')
    context_file = B.getConfigArchiveDir('pcce-random') + 'selected_contexts.txt' 
  elif 'fcce' in config or 'scce' in config:
    ofs.write('cat SCCE.out\n')
    context_file = B.getConfigArchiveDir('fcce-random') + 'selected_contexts.txt'
  ofs.close()

  print('start time:', str(datetime.now()))
  t1 = time.time()
  ret = OS.system('export CONTEXT_FILE=%s; cd %s; bash myrun.sh' % (context_file, iter_dir))
  t2 = time.time()
  print('end time:', str(datetime.now()))
  print("elapsed time: %.3f seconds, ret: %d\n" % ((t2-t1), ret))

def copyConfigIter(config, old_it, new_it, cat='ref'):
  for B in getBenches():
    old_iter_dir = B.getConfigDir(config, cat) + "/i%d/" % (old_it)
    new_iter_dir = B.getConfigDir(config, cat) + "/i%d/" % (new_it)
    OS.system("mkdir -p "+new_iter_dir)
    OS.system("cp -r %s/* %s" % (old_iter_dir, new_iter_dir))
