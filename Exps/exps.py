import time
from datetime import datetime

import Utils.OS as OS
from Global.bms import *

PERF = 'perf stat -o perf_stats.out -B -e cpu-cycles,instructions,cache-references,cache-misses,branches,branch-misses,page-faults,context-switches,cpu-migrations,minor-faults,major-faults,alignment-faults,emulation-faults,L1-dcache-loads,L1-dcache-load-misses,L1-dcache-stores,L1-dcache-store-misses,L1-dcache-prefetches,L1-dcache-prefetch-misses,L1-icache-loads,L1-icache-load-misses,LLC-loads,LLC-load-misses,LLC-stores,LLC-store-misses,LLC-prefetches,LLC-prefetch-misses,dTLB-loads,dTLB-load-misses,dTLB-stores,dTLB-store-misses,iTLB-loads,iTLB-load-misses,branch-loads,branch-load-misses'


# def run_exps(config, iters=1, prefix=PERF):
#     for B in CPU2006.cur:
#         ncmds = len(B.ref_cmds)
#         for cmd in range(ncmds):
#             run_exps_r(B, config, cmd, iters, prefix)

def run_exp(config, iters=range(1), env="", prefix=PERF):
    for i in iters:
        t1 = time.time()
        for B in CPU2006.cur:
            ncmds = len(B.ref_cmds)
            for cmd in range(ncmds):
                run_exps_i(B, config, cmd, i, prefix, env)
        t2 = time.time()
        print("iter %d takes %.3f seconds" % (i, t2-t1))
    OS.print_failed()


def exp1():
#    run_exps(['clone.2.sa'], range(1), PERF+' env XPS_Allocator=x XPS_ArenaMode=1')
    run_exps(['clone.2.de'], range(1), PERF+' env XPS_Allocator=j')
    run_exps(['clone.2.ma'], range(1), PERF+' env XPS_Allocator=x XPS_ArenaMode=-1')
#    run_exps(['clone.2.sa'], [1,2], PERF+' env XPS_Allocator=x XPS_ArenaMode=1')
    run_exps(['clone.2.de'], [1,2], PERF+' env XPS_Allocator=j')
    run_exps(['clone.2.ma'], [1,2], PERF+' env XPS_Allocator=x XPS_ArenaMode=-1')

        
def run_exps(configs, iters=range(1), runs=[], env="", prefix=PERF):
#    OS.clear_failed()
    for i in iters:
        t1 = time.time()
        for B in CPU2006.cur:
            print("runs", len(B.ref_cmds))
            if len(runs) == 0:
                runs = range(len(B.ref_cmds))
            print(runs)
            for run in runs:
                for cfg in configs:
                    run_exps_i(B, cfg, run, i, prefix, env)
                
        t2 = time.time()
        print("iter %d takes %.3f seconds" % (i, t2-t1))
 #   OS.print_failed()


def run_exps_i(bench, config, run, it, prefix, env):
    iter_dir = XPS.result_dir + bench.name + "-ref/" + config + "/i%d/r%d/"%(it,run)
    OS.system("mkdir -p %s" % (iter_dir))
    ofs = open(iter_dir+'run.sh', 'w')
    ofs.write('cp -r ../../../input/* .\n')
    
    run_cmd = prefix + ' env ' + env + ' ../../' + bench.name + ' ' + bench.ref_cmds[run]
    ofs.write(run_cmd+'\n')
    ofs.write('ls | grep -v ".out" | grep -v ".log" | grep -v ".err" | grep -v "run.sh" | xargs rm -r')
    ofs.close()

    print('start time:', str(datetime.now()))
    t1 = time.time()
    OS.system('cd %s; bash run.sh' % iter_dir)
    t2 = time.time()
    print('end time:', str(datetime.now()))
    print("elapsed time: %.3f seconds\n" % (t2-t1))
    
    #OS.exec('cd %s; ls | grep -v ".out" | grep -v ".log" | grep -v ".err" | xargs rm -r' % (iter_dir))

    
def do2():
    arches = ["i386", "pentium", "pentium2", "pentium4", "core2", "pentium-mmx"]
    run_exps(arches, [0,1,2])
