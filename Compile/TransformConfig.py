import json
import os
from Global.Bench import *
from Global.XPS import *
import Utils.OS as OS

class TransformConfig(object):
  """
  A class that represents transform configs

  [Members] (in order)
  * Compilation options such as -O3
  * Path to marena
  * When set, link to marena.a instead of marena.so (may not be up-to-date)
  * When set, recompile the target even though a previous build result is found

  """
  def __init__(self):
    self.name = ''
    self.input = ''
    self.output = ''
    self.passes = []
    self.binary = False
    self.redirect_stdout = False
    
  def load(self, cfg_name):
    self.name = cfg_name
    config_file = 'Configs/Transform/' + cfg_name + '.cfg'
    d = json.load(open(config_file, "r"))
    self.input = d['src']
    self.output = d.get('out', cfg_name)
    self.passes = d['passes']
    self.binary = int(d.get('binary', 0))

  def run(self):
    cfg = self.name
    for B in getBenches():
      source = B.getIR(self.input)
      opt = XPS.llvm_bin + "/opt"
      so_prefix = XPS.llvm_bin + "../lib/"

      assert type(self.passes) == list
      for p in self.passes:
        p = p.replace("$B", B.name)
        if '.so' in p:
          opt += " -load " + so_prefix + p
        else:
          opt += " " + p

      output = "-o " + B.getIR(self.output)
        
      if not self.binary:
        output = "-S " + output
      cmd = " ".join([opt, source, output])
      if self.redirect_stdout:
        cfg_dir = B.getResultDir() + out
        if not os.path.isdir(cfg_dir):
          OS.makedirs(cfg_dir)
        cmd += " > " + cfg_dir + "/pass.out"
      OS.system(cmd)

  @staticmethod
  def loadFromFile(cfg_name):
    config_file = 'Configs/Build/' + cfg_name + '.cfg'
    cfg = BuildConfig()
    d = json.load(open(config_file, "r"))
    cfg.comp_flags = d['comp_flags'].split()
    cfg.marena = d['marena']
    cfg.static = d['static']
    cfg.clean = d['clean']
    cfg.binary = d['binary']
    return cfg




    
