import re
from Global.Bench import *
import Utils.OS as OS
import os

def createExeMakefile(cfg, compiler_dir="/usr/bin/",
                      comp_flags="", linker_flags=""):
  '''
  Create a makefile that produces an executable. The
  makefile will be named as "makefile.cfg"
  '''
  for B in getBenches():
    template = B.getHomeDir("build/build_base_mytest-m64.0000/") + "make.out"
    makefile = B.getHomeDir("src") + "makefile." + cfg
    if not os.path.isfile(template):
      print("makefile not found: " + template)
      continue
      
    ifs = open(template)
    ofs = open(makefile, "w")

    content = ifs.read()
    placeholder = "{{origpath}}"
    if placeholder in content:
      origpath = XPS.orig_dir
      content.replace(placeholder, origpath)

    assert "-O3" in content

    content = re.sub(
      r'/bin/gcc',
      "%s %s" % (compiler_dir+"gcc", comp_flags),
      content
    )

    content = re.sub(
      r'/bin/gcc',
      "%s %s" % (compiler_dir+"gcc", linker_flags),
      content
    )

    content = re.sub(
      r'/bin/g++',
      "%s %s" % (compiler_dir+"g++", comp_flags),
      content
    )

    content = re.sub(
      r'/bin/g++',
      "%s %s" % (compiler_dir+"g++", linker_flags),
      content
    )

    ofs.write(content)

def createExeMakefile(cfg, compiler="/usr/bin/gcc",
                      comp_flags="", linker_flags=""):
  '''
  Create a makefile that produces an executable. The
  makefile will be named as "makefile.cfg"
  '''
  for B in getBenches():
    template = B.getHomeDir("src") + "makefile.template"
    makefile = B.getHomeDir("src") + "makefile." + cfg
    ifs = open(template)
    ofs = open(makefile, "w")

    content = ifs.read()
    placeholder = "{{origpath}}"
    if placeholder in content:
      origpath = XPS.orig_dir
      content.replace(placeholder, origpath)

    assert "-O2" in content

    content = re.sub(r'/usr/bin/gcc *-c',
           "%s -c %s" % (compiler, comp_flags),
           content)

    content = re.sub(r'/usr/bin/gcc *-g -rdynamic',
           "%s %s" % (compiler, linker_flags),
           content)

    ofs.write(content)

def foo():
  for B in getBenches():
    original = B.getHomeDir("src") + "makefile.original"
    template = B.getHomeDir("src") + "makefile.template"
    if os.path.isfile(original):
      OS.system("cp %s %s" % (original, template))

def replaceLinker(content):
  lines = content.split('\n')
  assert len(lines[-1]) == 0

  lines[-2] = re.sub(
      r'/usr/bin/gcc',
      "%s/llvm-link -S" % (XPS.llvm_bin),
      lines[-2]
    )
  lines[-2] = re.sub(
      r'/usr/bin/g\+\+',
      "%s/llvm-link -S" % (XPS.llvm_bin),
      lines[-2]
    )
  lines[-2] = re.sub(
      r'/bin/gcc',
      "%s/llvm-link -S" % (XPS.llvm_bin),
      lines[-2]
    )
  lines[-2] = re.sub(
      r'/bin/g\+\+',
      "%s/llvm-link -S" % (XPS.llvm_bin),
      lines[-2]
    )
  

  return '\n'.join(lines)

def createLinkFile(B, f):
  llvmbin = XPS.llvm_bin
  linker = llvmbin + 'clang++'
  
  template_link_cmd = linker + " -m64 -g -O3 -march=native -fno-unsafe-math-optimizations ${input} -o ${output} -lm"
  print(f)
  with open(f, "w") as ofs:
    print(template_link_cmd, file=ofs)

  
def createIRMakefile(comp_flags=""):
  '''
    Create a makefile that produces an IR. The
    makefile will be named as "makefile.cfg"
    '''
  for B in getBenches():    
    template = B.getHomeDir("build/build_base_mytest-m64.0000/") + "make.out"
    makefile = B.getHomeDir("mybuild") + "template_compile_to_ir.sh"
    link_file = B.getHomeDir("mybuild") + "template_link.sh"
    print(makefile)

    if not os.path.isfile(template):
      print("makefile not found: " + template)
      continue

    llvmbin = XPS.llvm_bin

    createLinkFile(B, link_file)
      
    ifs = open(template)
    ofs = open(makefile, "w")
    content = ifs.read()

    content = replaceLinker(content)
      
    assert "-O3" in content

    

    content = re.sub(
      r'/usr/bin/gcc',
      "%s/clang -emit-llvm %s" % (llvmbin, comp_flags),
      content
    )

    content = re.sub(
      r'/usr/bin/g\+\+',
      "%s/clang++ -emit-llvm %s" % (llvmbin, comp_flags),
      content
    )
    
    content = re.sub(
      r'/bin/gcc',
      "%s/clang -emit-llvm %s" % (llvmbin, comp_flags),
      content
    )

    content = re.sub(
      r'/bin/g\+\+',
      "%s/clang++ -emit-llvm %s" % (llvmbin, comp_flags),
      content
    )


    if B.isFortran():
      content = re.sub(r'/usr/bin/gfortran',
             "%s/flang -emit-llvm -Wl,-rpath=%s -lomp -L%s" %
             (XPS.llvm_bin, XPS.llvm_lib, XPS.llvm_lib), content)
      content = re.sub(r'/bin/gfortran',
             "%s/flang -emit-llvm -Wl,-rpath=%s -lomp -L%s" %
             (XPS.llvm_bin, XPS.llvm_lib, XPS.llvm_lib), content)
      
      pass

    ofs.write(content)


def makeIR():
  failed = []
  for B in getBenches():
    print("compile to ir %s..." % B.name)
    pre_cmd = ""
    if B.isFortran():
      pre_cmd = 'cd %s; . ./shrc;' % XPS.orig_dir

    src_dir = B.getHomeDir("src")
    ir_dir = B.getHomeDir("ir")
    if not os.path.isdir(ir_dir):
      os.makedirs(ir_dir)
    out = B.name + '.ll'

    ret = OS.system("%s cd %s; make -j 20 -f makefile.ir; mv %s %s" %
                    (pre_cmd, src_dir, B.getShortName(), out))

    if ret != 0:
      failed.append(B.name)

  if len(failed) > 0:
    print("failed:", failed)
