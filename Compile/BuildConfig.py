import json
import os
from Global.Bench import *
from Global.XPS import *
import Utils.OS as OS

class BuildConfig(object):
  """
  A class that represents build configs

  [Members] (in order)
  * Compilation options such as -O3
  * Path to marena
  * When set, link to marena.a instead of marena.so (may not be up-to-date)
  * When set, recompile the target even though a previous build result is found

  """

  def __init__(self):
    self.name = ''
    self.comp_flags = []
    self.marena = ''
    self.static = False

  def setDefaultConfig(self):
    self.comp_flags = ['-O3']
    self.marena = "/home/tzhou/projects/marenapp/build/"
    
  def load(self, cfg_name):
    self.name = cfg_name
    self.setDefaultConfig()
    config_file = 'Configs/Build/' + cfg_name + '.cfg'
    if not os.path.isfile(config_file):
      return
      
    d = json.load(open(config_file, "r"))
    self.comp_flags = d['comp_flags'].split()
    self.marena = d['marena']
    self.static = d.get('static', False)
    
  def getLinkCommand(self, B, input, output):
    f = B.getHomeDir('mybuild') + 'link.sh'
    if suite == CPU2006:
      f = B.getHomeDir('src') + 'link.sh'

    if not os.path.isfile(f):
      return ""
      
    cmd = open(f).read().strip()
    cmd = cmd.replace('${input}', input)
    cmd = cmd.replace('${output}', output)

    ld_flags = ' '
    if self.marena:
      if self.static:
        ld_flags += ' %s/libmarena1.a' % self.marena
      else:
        ld_flags += ' -lmarena -L {0} -Wl,-rpath={0}'.format(self.marena)

    if B.name == '507.cactuBSSN_r':
      ld_flags += ' -lflang'
    cmd += ld_flags
    return cmd

  def getDefaultCommand(self, B, input, output):
    llvmbin = XPS.llvm_bin
    compiler = llvmbin + 'clang'
    if B.isCpp():
      compiler = llvmbin + 'clang++'

    ld_flags = "-lm"
    if self.marena:
      self.marena = '/home/tzhou/projects/marenapp/build/'
      if self.static:
        ld_flags += ' %s/libmarena1.a' % self.marena
      else:
        ld_flags += ' -lmarena -L {0} -Wl,-rpath={0}'.format(self.marena)

    ## Specific code for Ubuntu VM
    #input += ' /usr/local/lib/libbacktrace.a'
    cmd = " ".join(
      [compiler, ' '.join(self.comp_flags) + " -g -rdynamic", ld_flags,
       input, "-o", output]
      )
    return cmd

  def run(self):
    '''
    It tries to find and run link.sh in each benchmark's `mybuild` directory.
    If not found it runs a default build/link command
    '''
    cfg = self.name
    for B in getBenches():
      cfg_dir = B.getConfigDir(cfg)
      OS.makedirs(cfg_dir)

      src = B.getIR(cfg)
      exe = cfg_dir + B.name

      cmd = self.getLinkCommand(B, src, exe)
      if not cmd:
        cmd = self.getDefaultCommand(B, src, exe)
      ret = OS.system(cmd)
