import os
import json
from Global.Bench import *
from Global.XPS import *
import Utils.OS as OS
from Compile.BuildConfig import *
from Compile.TransformConfig import *

def transform(*cfgs):
  runTransformConfigs(*cfgs)

def build(*cfgs, run_transform=False):
  if run_transform:
    runTransformConfigs(*cfgs)
  runBuildConfigs(*cfgs)
  
def runBuildConfigs(*cfgs):
  for cfg in cfgs:
    runBuildConfig(cfg)

def runBuildConfig(cfg_name):
  cfg = BuildConfig()
  cfg.load(cfg_name)
  cfg.run()
  
def runTransformConfigs(*cfgs):
  for cfg in cfgs:
    runTransformConfig(cfg)

def runTransformConfig(cfg_name):
  cfg = TransformConfig()
  cfg.load(cfg_name)
  cfg.run()

def copyInputs():
  for B in getBenches():
    data_dir = XPS.bench_dir + B.name + "/data/"
    ref_dir = B.getResultDir()
    OS.system("rm -rf "+ref_dir+"/inputs/ref")
    OS.system("mkdir -p %s/inputs/all" % (ref_dir))
    OS.system("mkdir -p %s/inputs/ref" % (ref_dir))
    OS.system("mkdir -p %s/inputs/test" % (ref_dir))

    #OS.system("cp -r %s/* %s/inputs/ref" % (B.getHomeDir('run/run_base_refrate_mytest-m64.0000'), ref_dir))
    
    OS.system("cp -r %s/all/input/* %s/inputs/all" % (data_dir, ref_dir))
    OS.system("cp -r %s/refrate/input/* %s/inputs/ref" % (data_dir, ref_dir))
    OS.system("cp -r %s/test/input/* %s/inputs/test" % (data_dir, ref_dir))
    
def copyConfig(old, new):
  for B in getBenches():
    old_dir = B.getConfigDir(old)
    new_dir = B.getConfigDir(new)
    OS.system("mkdir -p %s" % (new_dir))
    OS.system("cp -r %s/* %s" % (old_dir, new_dir))
    
def copyIter(config, src, dest):
  for B in getBenches():
    src_dir = B.getConfigIterDir(config, src)
    dest_dir = B.getConfigIterDir(config, dest)
    OS.system("mkdir -p %s" % (dest_dir))
    OS.system("cp -r %s/* %s" % (src_dir, dest_dir))




  
# ================================================ @deprecated starts here   
def createIRConfigs(cfgs, comp_flags="-O2", passes=None,
                    marena=True, static=False, clean=False):
  assert type(cfgs) == list
  for cfg in cfgs:
    createIRConfig(cfg, comp_flags, passes, marena, static, clean)

def createIRConfig(cfg, comp_flags="-O2", passes=None,
                        marena=True, static=False, clean=False):
  '''This function creates a configuration from an .ll file
  1. Create the config directory
  2. Compile the IR file to executable in the config directory
  '''
  config_file = "Configs/Build/%s.cfg" % cfg
  for B in getBenches():
    cfg_dir = B.getResultDir() + cfg + "/"
    if clean:
      OS.rm(cfg_dir)
    OS.makedirs(cfg_dir)

    src = B.getIR(cfg)
    exe = cfg_dir + B.name
    llvmbin = XPS.llvm_bin
    compiler = llvmbin + 'clang'
    if B.isCpp():
      compiler = llvmbin + 'clang++'

    so_prefix = XPS.llvm_bin + "../lib/"
    if passes:
      assert type(passes) == list

      opt = llvmbin + "/opt"
      for p in passes:
        p = p.replace("$B", B.name)
        opt += " -load " + so_prefix + p

      dest = cfg_dir + "tmp.ll"
      OS.system(" ".join([opt, src, "-S -o", dest]))
      src = dest

    ld_flags = "-lm"
    if marena:
      marena = '/home/tzhou/projects/marenapp/build/'
      if static:
        ld_flags += ' %s/libmarena1.a' % marena
      else:
        ld_flags += ' -lmarena -L {0} -Wl,-rpath={0}'.format(marena)


    ## Specific code for Ubuntu VM
    src += ' /usr/local/lib/libbacktrace.a'
    
    ret = OS.system(" ".join(
      [compiler, comp_flags+" -g -rdynamic", ld_flags, src, "-o", exe]
    ))

    if ret == 0:
      data = {}
      data['comp_flags'] = comp_flags
      data['passes'] = passes
      data['marena'] = marena
      data['clean'] = marena
      json.dump(data, open(config_file, "w"))


def runPasses(passes, src="", out="", binary=False, redirect_stdout=False):
  transform_file = "Configs/Transform/%s.cfg" % out
  data = {}
  data['passes'] = passes
  data['out'] = out
  data['src'] = src
  json.dump(data, open(transform_file, "w"))

  for B in getBenches():
    source = B.getIR('O2')
    if src:
      source = B.getIR(src)
    opt = XPS.llvm_bin + "/opt"
    so_prefix = XPS.llvm_bin + "../lib/"

    assert type(passes) == list
    for p in passes:
      p = p.replace("$B", B.name)
      opt += " -load " + so_prefix + p

    output = ""
    if out:
      output = "-o " + B.getIR(out)

    if not binary:
      output = "-S " + output
    cmd = " ".join([opt, source, output])
    if redirect_stdout:
      cfg_dir = B.getResultDir() + out
      if not os.path.isdir(cfg_dir):
        OS.makedirs(cfg_dir)
      
      cmd += " > " + cfg_dir + "/pass.out"
    OS.system(cmd)

def run(passes, src="", out="", binary=False, redirect_stdout=False):
  transform_file = "Configs/Transform/%s.cfg" % out
  data = {}
  data['passes'] = passes
  data['out'] = out
  data['src'] = src
  json.dump(data, open(transform_file, "w"))

  for B in getBenches():
    source = B.getIR('O2')
    if src:
      source = B.getIR(src)
    opt = XPS.llvm_bin + "/opt"
    so_prefix = XPS.llvm_bin + "../lib/"

    assert type(passes) == list
    for p in passes:
      p = p.replace("$B", B.name)
      opt += " -load " + so_prefix + p

    output = ""
    if out:
      output = "-o " + B.getIR(out)

    if not binary:
      output = "-S " + output
    cmd = " ".join([opt, source, output])
    if redirect_stdout:
      cfg_dir = B.getResultDir() + out
      if not os.path.isdir(cfg_dir):
        OS.makedirs(cfg_dir)
      
      cmd += " > " + cfg_dir + "/pass.out"
    OS.system(cmd)

# def runTranformConfig(*cfgs, redirect_stdout=False):
#   for p in cfgs:
#     dump_file = "Configs/Transform/%s.cfg" % p
#     args = json.load(open(dump_file, "r"))
#     runPasses(args['passes'], args['src'], args['out'], redirect_stdout=redirect_stdout)

    
def rerunPasses(*passes, redirect_stdout=False):
  for p in passes:
    dump_file = "Configs/Transform/%s.cfg" % p
    args = json.load(open(dump_file, "r"))
    runPasses(args['passes'], args['src'], args['out'], redirect_stdout=redirect_stdout)

def renameConfig(old, new):
  for B in getBenches():
    old_dir = B.getResultDir() + old + "/"
    new_dir = B.getResultDir() + new + "/"
    os.rename(old_dir, new_dir)

def removeConfig(cfg):
  for B in getBenches():
    d = B.getResultDir() + cfg
    try:
      OS.rmtree(d)
    except Exception as e:
      print(e)

def foo(cfg):
  for i in range(5):
    for B in getBenches():
      for run in B.getIterRuns(cfg, i):
        os.rename(run+"SCCE.out", run+"CCE.out")
      
def foo1():
  for B in getBenches():
    OS.system('rm -rf %s' % (XPS.result_dir+B.name+'/ref/'))

def foo2():
  for B in getBenches():
    for cfg in B.getConfigDirs():
#      print(cfg)
      OS.system('mv %s %s' % (cfg+'ref/'+B.name, cfg))
      # OS.makedirs(cfg+'ref')
      # OS.makedirs(cfg+'test')
      # OS.system('mv %s %s' % (cfg+'*', cfg+'ref'))
    
def removeBadConfigs():
  for B in getBenches():
    p = B.getResultDir()
    for item in os.listdir(p):
      if len(item) == 1:
        OS.rmtree(p+"/"+item)

