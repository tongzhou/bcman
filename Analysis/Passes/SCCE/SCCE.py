from Global.Bench import *
import Utils.OS as OS
from prettytable import PrettyTable
from Utils.xlsx import Chart
import numpy
import Analysis.Passes.PCCE.PCCE as PCCE

# [Format]
T = {}
H = []
C = {}
fields = [
#  'context level',
  'total functions',
  'total edges',
  'back edges',
  'max cc num',
  'internal sites',

]

field_name_map = {
  'context level': 'Context Level',
  'max cc num': 'Max CC Num',
  'internal sites': 'Internal Sites',
  'back edges': 'Back Edges',
  'total edges': 'Total Edges',
  'total functions': 'Functions',

}

field_cfg_map = {
  'context level': 'scce',
  'max cc num': 'pcce',
  'internal sites': 'scce',
  'back edges': 'pcce',
  'total edges': 'pcce',
  'total functions': 'pcce',
}

def getOutPutDir():
  dir = os.path.dirname(__file__) + "/archive/"
  if not os.path.isdir(dir):
    os.makedirs(dir)
  return dir

def init():
  global T, H, C
  field = 'bench'
  C[field] = []
  for B in getBenches():
    T[B.name] = []
    H = [field]
    C[field].append(B.name)

def run():  
  init()
  for f in fields:
    addPassColumn(field_cfg_map[f], f)
  dump()


def addPassColumn(cfg, field):
  global T, H, C
  H.append(field)
  C[field] = []
  for B in getBenches():
    file = B.getCfgDir(cfg) + '/pass.out'
    found = 0
    for line in open(file):

      if field+": " in line:
        key, value = line.strip().split(': ')
        C[field].append(int(value))
        T[B.name].append(int(value))
        found = 1
        break
    if not found:
      raise Exception(file + " has no <%s>" % field)
  print(C[field])
  

def addContextLevelColumn():
  global T, H, C
  field = 'context level'
  H.append(field)
  C[field] = []
  for B in getBenches():
    file = B.getCfgDir('scce') + '/pass.out'
    found = 0
    for line in open(file):
      if "max: " in line:
        key, value = line.strip().split(': ')
        C[field].append(int(value))
        T[B.name].append(int(value))
        found = 1
        break
    if not found:
      raise Exception(file + " has no <%s>" % field)

def addMaxCCNumColumn():
  global T, H, C
  field = 'max cc num'
  H.append(field)
  C[field] = []
  for B in getBenches():
    file = B.getCfgDir('pcce') + '/pass.out'
    found = 0
    for line in open(file):
      if "max cc num: " in line:
        key, value = line.strip().split(': ')
        C[field].append(int(value))
        T[B.name].append(int(value))
        found = 1
        break
    if not found:
      raise Exception(file + " has no <internal sites>")

def addinternalSiteColumn():
  global T, H, C
  field = 'internal edges'
  H.append(field)
  C[field] = []
  for B in getBenches():
    file = B.getCfgDir('scce') + '/pass.out'
    found = 0
    for line in open(file):
      if "internal sites: " in line:
        key, value = line.strip().split(': ')
        C[field].append(int(value))
        T[B.name].append(int(value))
        found = 1
        break
    if not found:
      raise Exception(file + " has no <internal sites>")



def dump():
  dumpxlsx()
  table = PrettyTable(H)
  print(H)

  for b in sorted(T):
    table.add_row([b] + T[b])
  print(table)
  print(table.get_html_string(),
        file=open(getOutPutDir() + "scce.html", "w"))

def dumpxlsx():
  chart = Chart(getOutPutDir() + "scce.xlsx")
  chart.add_column(['Bench'] + C['bench'])
  for f in fields:
    chart.add_column([field_name_map[f]] + C[f])
  chart.render()

    
def load(fn):
  for line in open(getOutPutDir() + fn):
    items = line.split()
    if len(items) == 3:
      print((float(items[1])), int(float(items[2])))


def clearTable():
  global T
  T.clear()
