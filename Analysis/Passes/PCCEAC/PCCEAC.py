from Global.Bench import *
import Utils.OS as OS
from prettytable import PrettyTable
import numpy

# [Format]
# Indexed by first bench name then config name
T = {}
H = []

def getOutPutDir():
  dir = os.path.dirname(__file__) + "/archive/"
  if not os.path.isdir(dir):
    os.makedirs(dir)
  return dir

def init():
  global T, H
  for B in getBenches():
    T[B.name] = []
    H = ["bench"]

def run():
  init()
  addMaxCCNumColumn()
  dump()


def addMaxCCNumColumn():
  global T, H
  H.append("max cc num")
  for B in getBenches():
    file = B.getCfgDir('pcce-ac') + '/pass.out'
    found = 0
    for line in open(file):
      if "max cc num: " in line:
        key, value = line.strip().split(': ')
        T[B.name].append(int(value))
        found = 1
        break
    if not found:
      raise Exception(file + " has no <internal sites>")

def addinternalSiteColumn():
  global T, H
  H.append("internal edges")
  for B in getBenches():
    file = B.getCfgDir('scce') + '/pass.out'
    found = 0
    for line in open(file):
      if "internal sites: " in line:
        key, value = line.strip().split(': ')
        T[B.name].append(int(value))
        found = 1
        break
    if not found:
      raise Exception(file + " has no <internal sites>")

def getInternalEdgeNum(B):
  file = B.getCfgDir('scce') + '/pass.out'
  for line in open(file):
    if "internal sites: " in line:
      key, value = line.strip().split(': ')
      return int(value)
  raise Exception(file + " has no <internal sites>")
  
def report():
  table = PrettyTable(['configs'] + cfgs)
  for B in getBenches():
    b = B.name
    row = [b]
    for cfg in cfgs:
      row.append(getIterAve(b, cfg))
      #row.append(getIterStd(b, cfg))
    #row = [b, T[b]["hcce"], T[b]["pcce"]]
    table.add_row(row)
  
  print(table.get_html_string())

def getOutPutDir():
  dir = os.path.dirname(__file__) + "/archive/"
  if not os.path.isdir(dir):
    os.makedirs(dir)
  return dir

def dump():
  table = PrettyTable(H)
  print(H)

  for b in T:

    table.add_row([b] + T[b])
  print(table)
  print(table.get_html_string(),
        file=open(getOutPutDir() + "pcce.html", "w"))

    # for B in getBenches():
  #   b = B.name
  #   row = [b]
  #   for cfg in cfgs:
  #     row.append(getIterAve(b, cfg))
  #     # row.append(getIterStd(b, cfg))
  #   # row = [b, T[b]["hcce"], T[b]["pcce"]]
  #   table.append(row)
  #
  #
  # print(tabulate(table, headers=makeHeader(), tablefmt="plain", floatfmt=".4f"),
  #       file=open(getOutPutDir() + fn, "w"))
  #
def load(fn):
  for line in open(getOutPutDir() + fn):
    items = line.split()
    if len(items) == 3:
      print((float(items[1])), int(float(items[2])))


def clearTable():
  global T
  T.clear()
