from Global.Bench import *
from Utils.misc import *
import Utils.OS as OS
from prettytable import PrettyTable
from Utils.xlsx import Chart
import numpy
import statistics
import Analysis.Passes.PCCE.PCCE as PCCE
import Analysis.Perf.Perf as Perf


# Columns are configs

T = {}
Configs = []

def reportConfigs(configs, iters, cat='ref', metric='time'):
  assert type(configs) == list
  global T, Configs
  Configs = configs
  for cfg in configs:
    T[cfg] = {}
    #addConfig(cfg, iters, cat, metric)
    if cfg == 'base':
      addConfig(cfg, range(5000,5005), cat, metric)
    else:
      addConfig(cfg, iters, cat, metric)

  dump(configs)
  
def addConfig(cfg, iters, cat, metric):
  global T
  for B in getBenches():
    if metric == 'time':
      T[cfg][B.name] = Perf.getIterTimes(B, cfg, iters)

def getOutPutDir():
  dir = os.path.dirname(__file__) + "/archive/"
  if not os.path.isdir(dir):
    os.makedirs(dir)
  return dir

def dump(cfgs):
  dumpxlsx(cfgs)
  table = PrettyTable(['Configs']+getBenchCol())
  for cfg in Configs:
    col = [cfg]
    for B in getBenches():
      col.append(getAve(T[cfg][B.name]))
    table.add_row(col)

  
    
  # for b in sorted(T):
  #   table.add_row([b] + T[b])
  print(table)
  # print(table.get_html_string(),
  #       file=open(getOutPutDir() + "scce.html", "w"))

def getChartName(cfgs):
  name = getOutPutDir()
  for cfg in cfgs:
    name += '_' + cfg
  name += '.xlsx'
  return name

def getAve(ls):
  return float(sum(ls)) / len(ls)

def getMean(ls):
  return getAve(ls)

def getStd(ls):
  return statistics.stdev(ls)

def getBenchCol():
  col = []
  for B in getBenches():
    col.append(B.name)
  return col

def dumpxlsx(cfgs):
  chart = Chart(getChartName(cfgs))

  chart.add_column(['Bench'] + getBenchCol() + ['Average'])
  for cfg in Configs:
    col = [cfg]
    for B in getBenches():
      col.append(getAve(T[cfg][B.name]))

    # Make a average row  
    col.append(getAve(col[1:]))
    chart.add_column(col)

  # ADD CONFIDENCE INTERVAL
  for cfg in Configs:
    col = [cfg+"_cl"]

    print(T['base'])
    for B in getBenches():
      basemean = getMean(T['base'][B.name])
      basestd = getStd(T['base'][B.name])
      expmean = getMean(T[cfg][B.name])
      expstd = getStd(T[cfg][B.name])
      ci = get95CI(basemean, expmean, basestd, expstd, 5)
      col.append(ci)

    print(col)
    # Make a average row  
    col.append(getAve(col[1:]))
    chart.add_column(col)


  chart.set_x_title('Benchmarks')
  chart.set_y_title('Execution Time (In Seconds)')
  chart.set_title('Instrumentation')
  chart.render()

            
def load(fn):
  for line in open(getOutPutDir() + fn):
    items = line.split()
    if len(items) == 3:
      print((float(items[1])), int(float(items[2])))


def clearTable():
  global T
  T.clear()
