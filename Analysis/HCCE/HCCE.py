from Global.Bench import *
import Utils.OS as OS
import Analysis.Perf.Perf as Perf
from tabulate import tabulate
import numpy

# [Format]
# Indexed by first bench name then config name
T = {}
cfgs = ["O2.ic", "ecce-2", "ecce-2-alloc", "pcce", "pcce-alloc"]
metric = 'time'

def run(iters=None):
  T.clear()
  if not iters:
    iters = [0]
  for iter in iters:
    for cfg in cfgs:
      aggregate(cfg, iter)

#  print(T['482.sphinx3'])

def aggregate(cfg, iter):
  global T
  for B in getBenches():
    if B.name not in T:
      T[B.name] = {}

    runs = []
    for run in B.getIterRuns(cfg, iter):
      if metric == "time":
        result = Perf.getTime(run)
      elif metric == "instructions":
        result = Perf.getInstructions(run)
      elif metric == "cpu-cycles":
        result = Perf.getCPUCycles(run)

      runs.append(result)

    addIterResult(B.name, cfg, sum(runs))
    #T[B.name][cfg] = sum(runs)/len(runs)

def addIterResult(b, cfg, result):
  if cfg not in T[b]:
    T[b][cfg] = []
  T[b][cfg].append(result)

def getIterAve(b, cfg):
  assert type(T[b][cfg]) == list
  return sum(T[b][cfg])/len(T[b][cfg])

def getIterStd(b, cfg):
  data = T[b][cfg]
  assert type(data) == list
  return numpy.std(data)

def makeHeader():
  header = []
  for c in cfgs:
    header.append(c)
    header.append("std")
  return header
  
def printTable():
  global T
  table = []
  for B in getBenches():
    b = B.name
    row = [b]
    for cfg in cfgs:
      row.append(getIterAve(b, cfg))
      row.append(getIterStd(b, cfg))
    #row = [b, T[b]["hcce"], T[b]["pcce"]]
    table.append(row)
  
  print(tabulate(table, headers=makeHeader(), tablefmt="simple"))

def clearTable():
  global T
  T.clear()
