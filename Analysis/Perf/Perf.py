
file = "perf_stats.out"

def getInstructions(run_dir):
  for line in open(run_dir+file):
    if "instructions" in line:
      return float(line.split()[0].replace(',', ''))
  raise Exception('')

def getTime(run_dir):
  for line in open(run_dir+file):
    if "time elapsed" in line:
      return float(line.split()[0])
  raise Exception('I know Python!')

def getCPUCycles(run_dir):
  for line in open(run_dir + file):
    if "cpu-cycles" in line:
      return float(line.split()[0])
  raise Exception()

def getIterInstructions(run_dir):
  for line in open(run_dir+file):
    if "instructions" in line:
      return float(line.split()[0].replace(',', ''))

def getIterTimes(B, cfg, iters):
  ret = []
  for i in iters:
    run_sum = 0
    for run in B.getIterRuns(cfg, i):
      run_sum += getTime(run)
    ret.append(run_sum)
  return ret

def getIterCPUCycles(run_dir):
  for line in open(run_dir + file):
    if "cpu-cycles" in line:
      return float(line.split()[0])
