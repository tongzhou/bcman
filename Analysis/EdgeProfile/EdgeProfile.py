from Global.Bench import *
import Utils.OS as OS
import Analysis.EdgeProfile.huffman as huffman

cfg = 'fcce-naive'
edge_freqs = {}

def run():
  aggregate()

def aggregate(i=0):
  global edge_freqs
  for B in getBenches():
    edge_freqs = {}
    for run in B.getIterRuns(cfg, i):
      readFile(B, run)
    writeToFile(B)    

def readFile(B, run_dir):
  global edge_freqs
  infile = run_dir+"/cycle_edge_freqs.out"
  if not os.path.isfile(infile):
    print(infile+" does not exit!")
    return

  for line in open(infile):
    items = line.strip().split()
    edge_id = int(items[0])
    freq = int(items[1])
    
    if edge_id not in edge_freqs:
      edge_freqs[edge_id] = 0
    edge_freqs[edge_id] += freq

def doWriteLine(ofs):
  for key in edge_freqs:
    print(key, edge_freqs[key], file=ofs)
    
def writeToFile(B):
  archive_dir = B.getConfigDir(cfg, 'ref') + "/archive/"
  
  if not os.path.isdir(archive_dir):
    OS.makedirs(archive_dir)
  f = archive_dir + "cycle_edge_freqs.out"
  print(f)
  ofs = open(f, "w")
  doWriteLine(ofs)
  ofs.close()
  return f

def doHuffman():
  for B in getBenches():
    f = B.getConfigDir(cfg, 'ref') + "/archive/cycle_edge_freqs.out"
    print(f)
    huffman.run(1, f)
