import math
import os

file = "/home/tzhou/projects/xps/results_530/403.gcc-ref/fcce-naive/i30/r0/cycle_edge_freqs.out"

class Node:
  def __init__(self, key, freq):
    self.key = key
    self.freq = freq
    self.encoding = ''
    self.left_child = None
    self.right_child = None
  
  def isLeaf(self):
    return self.left_child==None and self.right_child==None

def load(file):
  queue = []
  for line in open(file):
    items = line.split()
    node = Node(items[0], int(items[1]))
    queue.append(node)
  
  queue = sorted(queue, key=lambda x: x.freq)
  return queue
    
def run(bits, file):
  q = load(file)
  results = []
  if len(q) == 0:
    print("file empty or not found:", file)
    return
  
  if len(q) == 1:
    q[0].encoding = '0'
    results.append(q[0])
  else:
    q = buildTree(q)
    #total = q[0].freq
    #print(q[0].freq, total * 16)
    results = assign(q)

  xpsProcess(results)
  dump1(results, os.path.dirname(file)+'/huffman_coding.txt')
  report(results)

def xpsProcess(nodes):
  for node in nodes:
    node.encoding = '1' + node.encoding
  
def dump1(nodes, ofile):
  ofs = open(ofile, 'w')
  for node in nodes:
    print(node.key, node.encoding, file=ofs)
  ofs.close()
  
def buildTree(q):
  while len(q) > 1:
    newnode = merge(q[0], q[1])
    del q[0:2]
    q.append(newnode)
    q = sorted(q, key=lambda x: x.freq)
    # print(len(q))
  return q

def assign(q):
  encoding = {}
  results = []
  while len(q) > 0:
    n = q[0]
    n.left_child.encoding = n.encoding + '0'
    n.right_child.encoding = n.encoding + '1'
    del q[0]

    if n.left_child.isLeaf():
      results.append(n.left_child)
    else:
      q.append(n.left_child)

    if n.right_child.isLeaf():
      results.append(n.right_child)
    else:
      q.append(n.right_child)
  

  # for n in results:
  #   print(n.key, n.encoding)
  return results

def getSlotBits(n):
  slot_bits = math.ceil(math.log(n, 2))
  if slot_bits == 0:
    slot_bits = 1 # use at least 1 bit
  return slot_bits
  
def report(ls):
  cost = 0
  total_freq = 0
  largest_key = 0
  for n in ls:
    cost += len(n.encoding) * n.freq
    total_freq += n.freq
    if n.key != 'compound':
      if int(n.key) > largest_key:
        largest_key = int(n.key)
  slot_bits = getSlotBits(largest_key)
  old_cost = slot_bits * total_freq
  new_cost = cost
  print(largest_key, "old cost:", old_cost, "new cost:", new_cost, "slot bits:", slot_bits)
  print("new / old:", float(new_cost) / old_cost)
  
def merge(node1, node2):
  node = Node('compound', node1.freq + node2.freq)
  node.left_child = node1
  node.right_child = node2
  return node
    
def dump(q):
  for n in q:
    print(n.key, n.freq)
  
def sort(freq_map):
  return sorted(freq_map, key=lambda x: x[1])
