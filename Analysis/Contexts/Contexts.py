from Global.Bench import *
import Utils.OS as OS
import Analysis.Perf.Perf as Perf
import Analysis.Contexts.PCCE as PCCE
import Analysis.Contexts.SCCE as SCCE
from prettytable import PrettyTable
import numpy
import random

T = {}

def run(iter=None):
  global T
  T.clear()
  if not iter:
    iter = 0

  for cfg in ['pcce-random', 'fcce-random']:  
    for B in getBenches():
      contexts = {}
      for run in B.getIterRuns(cfg, iter):
        file = ''
        if 'pcce' in cfg:
          file = run + 'pcce_contexts_dump.out'
        if 'fcce' in cfg:
          file = run + 'fcce_contexts_dump.out'  
        for line in open(file):
          contexts[line.strip()] = 0

      outfile = B.getConfigArchiveDir(cfg) + 'selected_contexts.txt'
      ofs = open(outfile, "w")

      num = 100
      if num > len(contexts):
        num = len(contexts) / 5
      indexs = getRandomIndexs(len(contexts), num)
      print("indexs: ", len(indexs))
      for i,c in enumerate(contexts.keys()):
        if i in indexs:
          print(c, file=ofs)
        #if random.uniform(0, 1) > 0.8:
          #print(c, file=ofs)
      ofs.close()

def getRandomIndexs(size, num):
  assert size > num
  indexs = set()
  while 1:
    for i in range(size):
      if random.uniform(0, 1) > 0.5:
        if (len(indexs) < num):
          indexs.add(i)
        else:
          return indexs
      
def aggregate(cfg, iter):
  global T

  for B in getBenches():
    if B.name not in T:
      T[B.name] = {}

    runs = []
    for run in B.getIterRuns(cfg, iter):
      if metric == 'contexts':
        result = tools[cfg].getDistinctCtxNum(run)
      elif metric == 'pushed':
        result = tools[cfg].getPushedBytes(run)
      elif metric == 'checked':
        result = tools[cfg].getCheckedBytes(run)

      else:
        raise Exception
      runs.append(result)

    addIterResult(B.name, cfg, sum(runs))
    #T[B.name][cfg] = sum(runs)/len(runs)

def addIterResult(b, cfg, result):
  if cfg not in T[b]:
    T[b][cfg] = []
  T[b][cfg].append(result)

def getIterAve(b, cfg):
  assert type(T[b][cfg]) == list
  return sum(T[b][cfg])/len(T[b][cfg])

def getIterStd(b, cfg):
  data = T[b][cfg]
  assert type(data) == list
  return numpy.std(data)

def makeHeader():
  header = []
  for c in cfgs:
    header.append(c)

  return header
  
def report():
  table = PrettyTable(['configs'] + cfgs)
  for B in getBenches():
    b = B.name
    row = [b]
    for cfg in cfgs:
      row.append(getIterAve(b, cfg))
      #row.append(getIterStd(b, cfg))
    #row = [b, T[b]["hcce"], T[b]["pcce"]]
    table.add_row(row)
  
  print(table.get_html_string())

def getOutPutDir():
  dir = os.path.dirname(__file__) + "/archive/"
  if not os.path.isdir(dir):
    os.makedirs(dir)
  return dir

def dump(fn=""):
  assert T, "table is empty!"
  if not fn:
    fn = "contexts.html"
  table = PrettyTable(['configs'] + cfgs)
  for B in getBenches():
    b = B.name
    row = [b]
    for cfg in cfgs:
      row.append(getIterAve(b, cfg))
      # row.append(getIterStd(b, cfg))
    # row = [b, T[b]["hcce"], T[b]["pcce"]]
    table.add_row(row)

  print(table.get_html_string(),
        file=open(getOutPutDir() + fn, "w"))

    # for B in getBenches():
  #   b = B.name
  #   row = [b]
  #   for cfg in cfgs:
  #     row.append(getIterAve(b, cfg))
  #     # row.append(getIterStd(b, cfg))
  #   # row = [b, T[b]["hcce"], T[b]["pcce"]]
  #   table.append(row)
  #
  #
  # print(tabulate(table, headers=makeHeader(), tablefmt="plain", floatfmt=".4f"),
  #       file=open(getOutPutDir() + fn, "w"))
  #
def load(fn):
  for line in open(getOutPutDir() + fn):
    items = line.split()
    if len(items) == 3:
      print((float(items[1])), int(float(items[2])))


def clearTable():
  global T
  T.clear()
