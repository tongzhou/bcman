
from Global.Bench import *
import Utils.OS as OS
import Analysis.Perf.Perf as Perf
import Analysis.Contexts.PCCE as PCCE
import Analysis.Contexts.SCCE as SCCE
from Utils.xlsx import Chart
from prettytable import PrettyTable
import numpy
import random

T = {}

useTotalRow = True

def run(iter=None):
  global T
  T.clear()
  if not iter:
    iter = 0
  
  for B in getBenches():
    cfg_results = {}
    for cfg in ['pcce-random', 'ofcce-random', 'oscce-random']:
      # if cfg == 'fcce-random':
      #   iter = 10
      run_results = []
      for run in B.getIterRuns(cfg, iter):
        file = ''
        if 'pcce' in cfg:
          file = run + 'pcce_stack_histogram.out'
        if 'fcce' in cfg:
          file = run + 'fcce_stack_histogram.out'
        if 'scce' in cfg:
          file = run + 'fcce_stack_histogram.out'  

        run_results.append(getRunResult(file))
      iter_result = getIterResult(B, run_results)
      cfg_result = processIterResult(B, iter_result)
      cfg_results[cfg] = cfg_result

    cfg_results = sumConfigResults(cfg_results) 
    report(B, cfg_results)
    

def getRunResult(file):
  hist = {}
  for line in open(file):
    a, b = line.strip().split()
    bytes = int(a)
    freq = int(b)
    if 'pcce' in file or 'fcce' in file:
      hist[bytes+1] = freq
    if 'scce' in file:
      hist[bytes+1] = freq
      # if 'xalan' in file:
      #   print(bytes+2, freq)
      #   hist[bytes+2] = freq
      #   print(line, hist)
      # else:
        
      #   hist[bytes+1] = freq
      
  if 'scce' in file:
    offset = 0
    if 'xalan' in file or 'imagick' in file:
      offset = 1
      
    newhist = {}
    for key in hist:
      newhist[key+offset] = hist[key]
    hist = newhist
   
  return hist
      
def getIterResult(B, run_results):
  hist = {}
  for rr in run_results:
    for bytes, freq in rr.items():
      if bytes not in hist:
        hist[bytes] = 0
      hist[bytes] += freq
  return hist

def processIterResult(B, run_results):
  return run_results

def sumConfigResults(cfg_results):
  keys = []
  table = {}
  for cfg, bytes_hist in cfg_results.items():
    if len(bytes_hist.keys()) > len(keys):
      keys = bytes_hist.keys()
      
  for key in keys:
    table[key] = {}
    for cfg, bytes_hist in cfg_results.items():
      #if 'pcce' in cfg or 'fcce' in cfg:
      table[key][cfg] = bytes_hist.get(key, 0)

  return table
  
def getChartName(B):
  return os.path.dirname(__file__) + "/archive/" + B.name + '_WordsHistogram.xlsx'

def getTotalWords(ls):
  sum = 0
  #print(ls)
  for i in range(len(ls)):
    sum += i * ls[i]
  return sum
  
def report(B, table):
  chart = Chart(getChartName(B), B.name)
  bytes_col = ['Words']
  cfg_cols = {}

  for key, cfg_results in table.items():
    bytes_col.append(key)
    for cfg, value in cfg_results.items():      
      if cfg not in cfg_cols:
        cfg_cols[cfg] = [cfg]
      cfg_cols[cfg].append(value)

  if useTotalRow:
    chart.add_column(bytes_col + ["total words"])
  else:
    chart.add_column(bytes_col)
  
  
  for cfg, col in cfg_cols.items():
    print(cfg, sum(col[1:]))
      
    if useTotalRow:
      chart.add_column(col+[getTotalWords(col[1:])])
    else:
      chart.add_column(col)
  chart.set_x_title('Words')
  chart.set_y_title('Frequency')
  chart.set_title('Stack Size Histogram (Minimal Bits / 64)')
  chart.render()
  

def addIterResult(b, cfg, result):
  if cfg not in T[b]:
    T[b][cfg] = []
  T[b][cfg].append(result)

def getIterAve(b, cfg):
  assert type(T[b][cfg]) == list
  return sum(T[b][cfg])/len(T[b][cfg])

def getIterStd(b, cfg):
  data = T[b][cfg]
  assert type(data) == list
  return numpy.std(data)

def makeHeader():
  header = []
  for c in cfgs:
    header.append(c)

  return header
  
# def report():
#   table = PrettyTable(['configs'] + cfgs)
#   for B in getBenches():
#     b = B.name
#     row = [b]
#     for cfg in cfgs:
#       row.append(getIterAve(b, cfg))
#       #row.append(getIterStd(b, cfg))
#     #row = [b, T[b]["hcce"], T[b]["pcce"]]
#     table.add_row(row)
  
#   print(table.get_html_string())

def getOutPutDir():
  dir = os.path.dirname(__file__) + "/archive/"
  if not os.path.isdir(dir):
    os.makedirs(dir)
  return dir

def dump(fn=""):
  assert T, "table is empty!"
  if not fn:
    fn = "contexts.html"
  table = PrettyTable(['configs'] + cfgs)
  for B in getBenches():
    b = B.name
    row = [b]
    for cfg in cfgs:
      row.append(getIterAve(b, cfg))
      # row.append(getIterStd(b, cfg))
    # row = [b, T[b]["hcce"], T[b]["pcce"]]
    table.add_row(row)

  print(table.get_html_string(),
        file=open(getOutPutDir() + fn, "w"))

    # for B in getBenches():
  #   b = B.name
  #   row = [b]
  #   for cfg in cfgs:
  #     row.append(getIterAve(b, cfg))
  #     # row.append(getIterStd(b, cfg))
  #   # row = [b, T[b]["hcce"], T[b]["pcce"]]
  #   table.append(row)
  #
  #
  # print(tabulate(table, headers=makeHeader(), tablefmt="plain", floatfmt=".4f"),
  #       file=open(getOutPutDir() + fn, "w"))
  #
def load(fn):
  for line in open(getOutPutDir() + fn):
    items = line.split()
    if len(items) == 3:
      print((float(items[1])), int(float(items[2])))


def clearTable():
  global T
  T.clear()
