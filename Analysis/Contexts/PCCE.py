
file = "PCCE.out"

def getDistinctCtxNum(run_dir):
  for line in open(run_dir+file):
    if "hashes: " in line:
      key, value = line.strip().split(': ')
      return int(value)

  raise Exception(run_dir + "has no <hashes>")

def getPushedBytes(run_dir):
  for line in open(run_dir+file):
    if "pushed: " in line:
      key, value = line.strip().split(': ')
      return int(value)

  raise Exception(run_dir + "has no <pushed>")

def getCheckedBytes(run_dir):
  for line in open(run_dir+file):
    if "read ids: " in line:
      key, value = line.strip().split(': ')
      return int(value)

  raise Exception(run_dir + file + ": key doesn't exist")
