from Global.Bench import *
import Utils.OS as OS

def run():
  aggregate()

def aggregate():
  for B in getBenches():
    ics = {}
    funcs = {}
    for run in B.getIterRuns("ic-profile", 0):
      readFile(B, run, ics, funcs)
    writeToFile(B, ics, funcs)

def readFile(B, run_dir, ics, funcs):
  infile = run_dir+"/ic_profile.out"
  if not os.path.isfile(infile):
    print(infile+" does not exit!")
    return

  for line in open(infile):
    items = line.strip().split()
    if line.startswith('ic:'):
      id, addr = items[1:]
      if id not in ics:
        ics[id] = []
      ics[id].append(addr)
    elif line.startswith('fn:'):
      addr, name = items[1:]
      funcs[addr] = name
    else:
      raise Exception("bad line: "+line)

def writeToFile(B, ics, funcs):
  archive_dir = B.getConfigDir("ic-profile") + "/archive/"
  if not os.path.isdir(archive_dir):
    OS.makedirs(archive_dir)
  f = archive_dir + "ic_profile.out"
  ofs = open(f, "w")

  unknown_addrs = set()
  for key in ics:
    for addr in ics[key]:
      if addr in funcs:
        print(key, funcs[addr], file=ofs)
      else:
        unknown_addrs.add(addr)

  ofs.close()
  if unknown_addrs:
    print("unrecognized addrs: ", unknown_addrs)

#
# def aggregate():
#   for B in getBenches():
#     archive_dir = B.getCfgDir("ic-profile") + "/archive/"
#     if not os.path.isdir(archive_dir):
#       OS.makedirs(archive_dir)
#     f = archive_dir + "ic_profile.out"
#     ofs = open(f, "w")
#     ofs.close()
#     ofs = open(f, "a")
#     for run in B.getIterRuns("ic-profile", 0):
#       readFile(B, run, ofs)
#     ofs.close()
#
# def readFile(B, run_dir, ofs):
#   ics = {}
#   funcs = {}
#   infile = run_dir+"/ic_profile.out"
#   if not os.path.isfile(infile):
#     print(infile+" does not exit!")
#     return
#
#   for line in open(infile):
#     items = line.strip().split()
#     if line.startswith('ic:'):
#       id, addr = items[1:]
#       if id not in ics:
#         ics[id] = []
#       ics[id].append(addr)
#     elif line.startswith('fn:'):
#       addr, name = items[1:]
#       funcs[addr] = name
#     else:
#       raise Exception("bad line: "+line)
#
#   unknown_addrs = set()
#   for key in ics:
#     for addr in ics[key]:
#       if addr in funcs:
#         print(key, funcs[addr], file=ofs)
#       else:
#         unknown_addrs.add(addr)
#
#   if unknown_addrs:
#     print("unrecognized addrs: ", unknown_addrs)
