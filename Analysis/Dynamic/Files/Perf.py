
file = "perf_stats.out"

def getInstructions(run_dir):
  for line in open(run_dir+file):
    if "instructions" in line:
      return float(line.split()[0].replace(',', ''))

def getTime(run_dir):
  for line in open(run_dir+file):
    if "time elapsed" in line:
      return float(line.split()[0])

def getCPUCycles(run_dir):
  for line in open(run_dir + file):
    if "cpu-cycles" in line:
      return float(line.split()[0])


