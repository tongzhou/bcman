from Global.Bench import *
import Utils.OS as OS
import Analysis.Dynamic.Files.Perf as Perf
import Analysis.Dynamic.Files.PCCE as CCE
from prettytable import PrettyTable
import numpy

# [Format]
# Indexed by fields

# (metric = xxx)
# Bench  Config1  Config2
# xxx    xxx      xxx
T = {}

cfgs = ["pcce", "ecce-2"]

metric = 'pushed'

method_table = {
  'time': Perf.getTime,
  'contexts': CCE.getDistinctCtxNum,
  'pushed': CCE.getPushedBytes,
  'checked': CCE.getCheckedBytes,
}

def run(iters=None):
  T.clear()
  if not iters:
    iters = [0]
  for iter in iters:
    for cfg in cfgs:
      aggregate(cfg, iter)

def getBenchCol():
  ls = ["Bench"]
  for B in getBenches():
    ls.append(B.name)
      
def aggregate(cfg, iter):
  global T

  for B in getBenches():
    runs = []
    for run in B.getIterRuns(cfg, iter):
      result = method_table[metric](run)
      runs.append(result)

    addIterResult(B.name, cfg, sum(runs))
    #T[B.name][cfg] = sum(runs)/len(runs)

def addIterResult(b, cfg, result):
  if cfg not in T[b]:
    T[cfg][b] = []
  T[cfg][b].append(result)

def getIterAve(b, cfg):
  ls = T[cfg][b]
  assert type(ls) == list
  return sum(ls)/len(ls)

def getIterStd(b, cfg):
  ls = T[cfg][b]
  assert type(ls) == list
  return numpy.std(ls)

def makeHeader():
  header = []
  for c in cfgs:
    header.append(c)
  return header

def getOutName():
  getOutPutDir() + metric + "_" + "_".join(cfgs) + ".xlsx"
  
def report(std=True):
  chart = Chart(getOutName())
  chart.add_column(getBenchCol())
  for cfg in cfgs:
    ave_col = [cfg]
    std_col = [cfg+"_std"]
    for B in getBenches():
      ave_col.append(getIterAve(B.name, cfg))
      std_col.append(getIterStd(b, cfg))
    chart.add_column(ave_col)

    if std:
      chart.add_column(std_col)
  chart.render()

def getOutPutDir():
  dir = os.path.dirname(__file__) + "/archive/"
  if not os.path.isdir(dir):
    os.makedirs(dir)
  return dir

def dump(fn=""):
  assert T, "table is empty!"
  if not fn:
    fn = "contexts.html"
  table = PrettyTable(['configs'] + cfgs)
  for B in getBenches():
    b = B.name
    row = [b]
    for cfg in cfgs:
      row.append(getIterAve(b, cfg))
      # row.append(getIterStd(b, cfg))
    # row = [b, T[b]["hcce"], T[b]["pcce"]]
    table.add_row(row)

  print(table.get_html_string(),
        file=open(getOutPutDir() + fn, "w"))

    # for B in getBenches():
  #   b = B.name
  #   row = [b]
  #   for cfg in cfgs:
  #     row.append(getIterAve(b, cfg))
  #     # row.append(getIterStd(b, cfg))
  #   # row = [b, T[b]["hcce"], T[b]["pcce"]]
  #   table.append(row)
  #
  #
  # print(tabulate(table, headers=makeHeader(), tablefmt="plain", floatfmt=".4f"),
  #       file=open(getOutPutDir() + fn, "w"))
  #
def load(fn):
  for line in open(getOutPutDir() + fn):
    items = line.split()
    if len(items) == 3:
      print((float(items[1])), int(float(items[2])))


def clearTable():
  global T
  T.clear()
