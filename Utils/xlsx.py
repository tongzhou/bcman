import xlsxwriter
import string


class Chart(object):
  def __init__(self, bookname='chart.xlsx', sheetname='Sheet1'):
    self.workbook = xlsxwriter.Workbook(bookname)
    self.sheet_name = sheetname
    self.worksheet = self.workbook.add_worksheet(sheetname)
    # Create a new Chart object.
    self.chart = self.workbook.add_chart({'type': 'column'})
    self.worksheet.insert_chart('H3', self.chart, {'x_scale': 2, 'y_scale': 2})
    self.data = []
    self.title = ''
    self.x_title = "x title"
    self.y_title = "y title"

  def add_column(self, column):
    self.data.append(column)

  def set_title(self, name):
    self.title = name
    
  def set_x_title(self, name):
    self.x_title = name

  def set_y_title(self, name):
    self.y_title = name

  def add_ave_row(self):
    for i, col in enumerate(self.data):
      if i == 0:
        self.data[i].append('average')
      else:
        ave = sum(col[1:]) / len(col[1:])
        self.data[i].append(ave)

  def render(self):    
    for i in range(len(self.data)):
      column = self.data[i]
      letter = string.ascii_uppercase[i]
      self.worksheet.write_column(letter + '1', column)

    for i, column in enumerate(self.data):
      if i == 0:
        continue

      letter = string.ascii_uppercase[i]
      self.chart.add_series({
        'name': '={1}!${0}$1'.format(letter, self.sheet_name),
        'categories': '={1}!$A$2:$A${0}'.format(len(column) + 1, self.sheet_name),
        'values': '={2}!${0}$2:${0}${1}'.format(letter, len(column) + 1, self.sheet_name),
        # 'y_error_bars': {
        #     'value': 0.2,
        #     'type': 'standard_error',
        # },

      })
      print({
        'name': '={1}!${0}$1'.format(letter, self.sheet_name),
        'categories': '={1}!$A$2:$A${0}'.format(len(column) + 1, self.sheet_name),
        'values': '={2}!${0}$2:${0}${1}'.format(letter, len(column) + 1, self.sheet_name),
      })

    self.chart.set_title({'name': self.title})
    self.chart.set_y_axis({'name': self.y_title})
    self.chart.set_x_axis({'name': self.x_title, 'num_font': {'rotation': 45}})
    self.workbook.close()

  @staticmethod
  def test1():
    c = Chart('test.xlsx')
    c.add_column(['a', 'b', 'c'])
    c.add_column([1, 2, 3])
    c.render()
