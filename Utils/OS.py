import os
import shutil

failed = []
fake = False
print_cmd = True

def makedirs(path):
    if print_cmd:
        print("<OS>: mkdir -p %s" % path)
    if fake:
        return
    os.system("mkdir -p %s" % path)

def remove(path):
    if print_cmd:
        print("<OS>: rm %s" % path)
    if fake:
        return
    os.remove(path)

def rmtree(path):
    if print_cmd:
        print("<OS>: rm -r %s" % path)
    if fake:
        return
    shutil.rmtree(path)

def rm(path):
    if print_cmd:
        print("<OS>: rm -r %s" % path)
    if fake:
        return
    system("rm -rf %s" % path)

def system(cmd):
    if print_cmd:
        print("<OS>: %s" % cmd)
    if fake:
        return
    return os.system(cmd)

# def exec(cmd):
#     ret = 0
#     if FakeOS.emCmd:
#         print("execute: " + cmd)
#     else:
#         if PrintOS.emCmd:
#             print("execute: " + cmd)
#         ret = os.system(cmd)
#     if ret != 0:
#         OS.failed.append(cmd)
#     return ret
#
#
# def print_failed():
#     if len(OS.failed) == 0:
#         return
#
#     print("failed commands:")
#     for cmd in OS.failed:
#         print(cmd)
#     OS.failed.clear()
#
#
# def clear_failed():
#     OS.failed = []
