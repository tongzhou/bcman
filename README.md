This is a collection of IPython-based scripts to transform, build, and run LLVM IRs
for SPEC CPU2006 and SPEC CPU2017.

# Setup
- Benchmark
  - Please copy my current benchmark directories. It has the structures the scripts need
  and pre-compiled IRs.
- Marena
  - Path is configured in xps.cfg
- LLVM commands
  - Path (`llvm_bin`) is configured in xps.cfg
- LLVM library
  - Path (`llvm_lib`) is configured in xps.cfg
  - My pass shared libraries are placed together with llvm libraries (since my pass files
  are compiled together with LLVM and clang). But if your passes are compiled in a standalone
  style, you'd need to edit `Compile/TransformConfig.py` to configure where the script can
  find .so for your passes.
- Results directory
  - Path (`cpu2017_result_dir`) is configured in xps.cfg

# Start the shell
```
$ bash shell.sh

# or
$ ipython -i loadme.py
```

# Set benchmarks
Benchmarks are set separately as a global list by running
```
: sb('gcc', 'mcf', 'xalan')       # alias for `setBenches` 
```
No need to type full name like `502.gcc`. Only a partial name for a benchmark suffices.
To remove some benchmarks, do `removeBenches()`.

# Setup input files
```
: copyInputs()
```
This copies all input files for a benchmark to its result directory (which will be used during
running).

# Make your config file
By default there is a base IR in every benchmark's $benchHome/ir, you can make a config file
to specify how the transformation is run. The following shows an example from
`Configs/Transform/pcce.cfg`:
```
{"src": "base", "passes": ["XPSPCCE.so -pcce -print-stats -acyclic-mode=0"]}
```
By default, "base" refers to IR $bench.base.ll. Like for 502.gcc, it'll be `502.gcc.base.ll`.
The config file specifies the input IR, and the passes to run. Each pass is a string of the
name of the shared library for that pass and its arguments.

By default, "xyz.cfg" generates an IR named "$bench.xyz.ll" in $benchHome/ir.

The dispatch happens in `Compile/Compile.py`.

To run the transformation in ipython, do

```
: transform('xyz.cfg') # generates $bench.xyz.ll
```

To compile `$bench.xyz.ll`, do
```
: build('xyz.cfg') # looks for and runs link.sh in $benchHome/mybuild
```

To run a config, do
```
: runExps(['xyz.cfg'], iters=[0], cat='ref')
```
Results are in `cpu2017_result_dir/$bench/$config/ref`.